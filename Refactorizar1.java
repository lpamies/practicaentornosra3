package refactorizar1;
/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */



import java.util.Scanner;
/*
 * 
 * Solo hay que poner el import del scanner que es lo unico que realmente estamos importando
 */


/** 
 * @author fvaldeon
 * @since 31-01-2018
 */

//El nombre de la clase debe ser mayuscula

public class Refactorizar1 {
	
	//Hay que ponerle un nombre adecuado que nos especifique que es exactamente
	final static String SALUDO = "Bienvenido al programa";

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//El Scanner no puede llamarse c puede llamarse input o lector pero nunca una letra suelta
		
		Scanner lector = new Scanner(System.in);
		
		System.out.println(SALUDO);
		
		System.out.println("Introduce tu dni");
		//Cuando hacemos un syso podemos aprovechar para dar nombre al String
		String DNI = lector.nextLine();
		System.out.println("Introduce tu nombre");
		String nombre = lector.nextLine();
	
		/*
		 * Las variables se escriben una en cada linea
		 * ademas en los tipos Int (y en todos) no deberiamos llamarlas a y b si no de otra forma, por ejemplo
		 * numero y numero2
		 * 
		 * Y deben declarar al momento de uso, y son magic numbers
		 */
	
		int numero=7; 
		int numero2=16;
		int numero3=25;
		
		//Hay que poner los parentesis cuando sea necesario
		if(numero > numero2 || (numero3 % 5) != 0 && ((numero3 * 3) - 1) > (numero2 / numero3)){
			
			System.out.println("Se cumple la condici�n");
		}
		
		//Poner parentesis para especificar que operaci�n tiene prioridad
		numero3 = numero + (numero2 * numero3) + (numero2 / numero);
		
		/*
		 * La declaracion correcta de una array es  String[] array los corchetes siempre se colocan 
		 * entre su tipo de dato y el nombre que le damos
		 * Al ser un array de los dias de la semana y que el valor de cada uno no va a cambiar se deben escribir
		 * todos en el mismo sitio
		 * 
		 *  El nombre del Array hay que cambiarlo por un nombre que sea m�s especifico
		 */
		
		String[] arrayDias = {"Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"};
		
		//El nombre del metodo tampoco nos dice a que se refiere 
		
		mostrarDias(arrayDias);
		
	}
	//Los corchetes estan mal y tambien las llaves, el comienzo de la llave siempre debe estar detras de un parentesis
	static void mostrarDias(String[] vectorStrings){
		
		/*La variable del for debe ser una letra (normalmente solemos poner "i" y en vez de poner un numero podemos poner la variable.length 
		* y as� recorrerlo hasta que se termine el vector
		*/
		for(int i = 0; i < vectorStrings.length; i++) {
			
			System.out.println("El dia de la semana en el que"
			
					//Aqui debemos poner la letra que pusimos en el for
					+ "te encuentras [" + (i + 1) +" -7] es el dia: " 
			
					+ vectorStrings[i]);
		}
	}
}