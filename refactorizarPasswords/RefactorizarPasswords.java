package refactorizarPasswords;

import java.util.Scanner;

public class RefactorizarPasswords {
	
public static void main(String[] args) {
	
			Scanner scanner = new Scanner(System.in);
			generarMenu();
			System.out.println("Introduce la longitud de la cadena: ");
			int longitud = scanner.nextInt();
			
			System.out.println("Elige tipo de password: ");
			int opcion = scanner.nextInt();
			
			String password = generarContra(longitud, opcion);

			System.out.println(password);
			scanner.close();
		}
/**
 * 
 * @param longitud
 * @param opcion
 * @return
 */
public static String generarContra(int longitud, int opcion) {
	String password = "";
	for (int i = 0; i < longitud; i++) {
	switch (opcion) {
	case 1:
		
		password += letraAleatoria();
		
		break;
	case 2:
		
		password += cifraAleatoria();
		
		break;
	case 3:
		
		password += letraYCaracter();
		
		
		break;
	case 4:
		
		password += letraNumeroCaracter();
		
		break;
	}
	
		
	}
	return password;
	
}
/**
 * 
 * @return
 */
public static char letraNumeroCaracter() {
	int n;
	n = (int) (Math.random() * 3);
	if (n == 1) {
		return letraAleatoria();
	} else if (n == 2) {
		char caracter4;
		caracter4 = caracterEspecialAlea();
		
	} else {
		int numero4;
		numero4 = cifraAleatoria();
		password += numero4;
	}
	
}
/**
 * 
 * @return
 */
public static char caracterEspecialAlea() {
	return (char) ((Math.random() * 15) + 33);
}

public static char letraYCaracter() {
	int n;
	n = (int) (Math.random() * 2);
	if (n == 1) {
		return letraAleatoria();
	} else {
		
		return caracterEspecialAlea();
		
	}
	
}
/**
 * 
 * @return
 */
public static char cifraAleatoria() {
	 
	
	return (char) (Math.random() * 10);
}
/**
 * 
 * @return
 */
public static char letraAleatoria() {
	 
	
	return (char) ((Math.random() * 26) + 65);
}
/**
 * 
 */
public static void generarMenu() {
	System.out.println("Programa que genera passwords de la longitud indicada, y del rango de caracteres");
	System.out.println("1 - Caracteres desde A - Z");
	System.out.println("2 - Numeros del 0 al 9");
	System.out.println("3 - Letras y caracteres especiales");
	System.out.println("4 - Letras, numeros y caracteres especiales");
}

	}


