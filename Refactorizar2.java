package refactorizar2;
/*
 *Hay que poner el paquete siempre para que funcione 
 */
/*
	 * Clase para refactorizar
	 * Codificacion UTF-8
	 * Paquete: ra3.refactorizacion
	 * Se deben comentar todas las refactorizaciones realizadas,
	 * mediante comentarios de una linea o de bloque.
	 */

	
	import java.util.Scanner;
	/*
	 * Hay que importar solo lo que se vaya a usar en este caso es el Scanner
	 * 
	 */
	/**
	 * 
	 * @author fvaldeon
	 * @since 31-01-2018
	 */
	public class Refactorizar2 {

		// El scanner nunca sera final static
		
		public static void main(String[] args) 
		{
		
			// Esta mal declarado, hay que poner "Scanner" antes de la "a" y el Scanner no puede llamarse con una letra
			Scanner input = new Scanner(System.in);
			
			
			//Las variables tienen que estar cada una en una linea y la variable "n" no especifica nada
			//Ni tampoco se usa en el programa por lo tanto no nos haria falta as� que podemos quitarlo (lo comento)
			
			//int n;
			int cantidad_maxima_alumnos;
			
			cantidad_maxima_alumnos = 10;
			
			//Los corchetes deben ir despues del int, ademas hay que ponerle un nombre que especifique 
			
			int[] arraysNotas = new int[10];
			
			/*Dentro del for despues del parentesis hay que escribir primero int y 
			*luego el resto del contenido, ademas a esa variable habr�a que llamarla "i"
			*/ 
			
			for(int i = 0; i < 10; i++)
			{
				/*Hay que escribir correctamente el nombre del scanner,
				* ademas que nota es n�mero por lo tanto hay que leer numeros(int) no caracteres ni cadenas
				*Por lo tanto hay que poner nextInt
				*/
				System.out.println("Introduce nota media de alumno");
				arraysNotas[i] = input.nextInt();
			}	
			
			System.out.println("El resultado es: " + recorrer_array(arraysNotas));
			
			input.close();
		}
		//El corchete tiene que estar despu�s del parentesis
		static double recorrer_array(int vector[]){
			//No hay que llamar a las variables con letras hay que ponerle un nombre espec�fico
			double numero = 0;
			
			//La variable del int hay que llamarla "i"
			for(int i = 0; i < 10; i++) 
			{
				numero = numero + vector[i];
			}
			return numero/10;
		}
		
	}