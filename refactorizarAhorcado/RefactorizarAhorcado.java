package refactorizarAhorcado;

import java.io.File;
import java.util.Scanner;

public class RefactorizarAhorcado {

		private final static byte NUM_PALABRAS = 20;
		private final static byte FALLOS = 7;
		private static String[] palabras = new String[NUM_PALABRAS];

		public static void main(String[] args) {

			String palabraSecreta;
			controlarErrores();

			Scanner input = new Scanner(System.in);

			palabraSecreta = generarPalabraAleatoria();

			char[][] caracteresPalabra = Palabra(palabraSecreta);

			String caracteresElegidos = "";
			int fallos;
			boolean acertado;
			
			caracteresElegidos = comienzoDelJuego(palabraSecreta, input, caracteresPalabra, caracteresElegidos);

			input.close();
		}
		
		private static String comienzoDelJuego(String palabraSecreta, Scanner input, char[][] caracteresPalabra,
				String caracteresElegidos) {
			// TODO Auto-generated method stub
			return null;
		}

		public static String generarPalabraAleatoria() {
			String palabraSecreta;
			palabraSecreta = palabras[(int) (Math.random() * NUM_PALABRAS)];
			return palabraSecreta;
		}
		
		
		
		public static char[][] Palabra(String palabraSecreta) {
			char[][] caracteresPalabra = new char[2][];
			caracteresPalabra[0] = palabraSecreta.toCharArray();
			caracteresPalabra[1] = new char[caracteresPalabra[0].length];
			return caracteresPalabra;
		}

		

		

		private static String comienzoAhorcado(String palabraSecreta, Scanner input, char[][] caracteresPalabra,
				String caracteresElegidos) {
			int fallos;
			boolean acertado;
			System.out.println("Acierta la palabra");
			do {

				System.out.println("####################################");

				caracterDePalabra(caracteresPalabra);
				
				System.out.println();

				caracteresElegidos = introduccionCaracter(input, caracteresElegidos);
				fallos = 0;

				fallos = caracterEncontrado(caracteresPalabra, caracteresElegidos, fallos);

				mostrarJuego(fallos);

				acertado = aciertosYFallos(palabraSecreta, caracteresPalabra, fallos);

			} while (!acertado && fallos < FALLOS);
			return caracteresElegidos;
		}
		public static void controlarErrores() {
			String palabraSecreta, ruta = "src\\palabras.txt";

			File fich = new File(ruta);
			Scanner inputFichero = null;

			try {
				inputFichero = new Scanner(fich);
				for (int i = 0; i < NUM_PALABRAS; i++) {
					palabras[i] = inputFichero.nextLine();
				}
			} catch (Exception e) {
				System.out.println("Error al abrir fichero: " + e.getMessage());
			} finally {
				if (fich != null && inputFichero != null)
					inputFichero.close();
			}
		}
		private static String introduccionCaracter(Scanner input, String caracteresElegidos) {
			System.out.println("Introduce una letra o acierta la palabra");
			System.out.println("Caracteres Elegidos: " + caracteresElegidos);
			caracteresElegidos += input.nextLine().toUpperCase();
			return caracteresElegidos;
		}

		private static void caracterDePalabra(char[][] caracteresPalabra) {
			for (int i = 0; i < caracteresPalabra[0].length; i++) {
				if (caracteresPalabra[1][i] != '1') {
					System.out.print(" -");
				} else {
					System.out.print(" " + caracteresPalabra[0][i]);
				}
			}
		}

		

		private static boolean aciertosYFallos(String palabraSecreta, char[][] caracteresPalabra, int fallos) {
			boolean acertado;
			if (fallos >= FALLOS) {
				System.out.println("Has perdido: " + palabraSecreta);
			}
			acertado = true;
			for (int i = 0; i < caracteresPalabra[1].length; i++) {
				if (caracteresPalabra[1][i] != '1') {
					acertado = false;
					break;
				}
			}
			if (acertado)
				System.out.println("Has Acertado ");
			return acertado;
		}
		private static int caracterEncontrado(char[][] caracteresPalabra, String caracteresElegidos, int fallos) {
			boolean encontrado;
			for (int j = 0; j < caracteresElegidos.length(); j++) {
				encontrado = false;
				for (int i = 0; i < caracteresPalabra[0].length; i++) {
					if (caracteresPalabra[0][i] == caracteresElegidos.charAt(j)) {
						caracteresPalabra[1][i] = '1';
						encontrado = true;
					}
				}
				if (!encontrado)
					fallos++;
			}
			return fallos;
		}
		private static void mostrarJuego(int fallos) {
			switch (fallos) {
			case 1:

				System.out.println("     _");
				break;
			case 2:

				System.out.println("      |");
				System.out.println("      |");
				System.out.println("      |");
				System.out.println("     _");
				break;
			case 3:
				System.out.println("  __ ");
				System.out.println("      |");
				System.out.println("      |");
				System.out.println("      |");
				System.out.println("     _");
				break;
			case 4:
				System.out.println("  __ ");
				System.out.println(" |    |");
				System.out.println("      |");
				System.out.println("      |");
				System.out.println("     _");
				break;
			case 5:
				System.out.println("  __ ");
				System.out.println(" |    |");
				System.out.println(" O    |");
				System.out.println("      |");
				System.out.println("     _");
				break;
			case 6:
				System.out.println("  __ ");
				System.out.println(" |    |");
				System.out.println(" O    |");
				System.out.println(" T    |");
				System.out.println("     _");
				break;
			case 7:
				System.out.println("  __ ");
				System.out.println(" |    |");
				System.out.println(" O    |");
				System.out.println(" T    |");
				System.out.println(" A   _");
				break;
			}
		}
		
	}